from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.
class Story7TestCase(TestCase):
    
    def setUp(self):
        self.client = Client()
    
    def test_url_exists(self):
        resp = self.client.get('/story_7/')
        self.assertTemplateUsed('index__story7.html')
        self.assertEqual(resp.status_code, 200)
        self.assertIn(b'Hello Apa Kabar?', resp.content)


class E2ETestCase(TestCase):

    def setUp(self):
        super(type(self), self).setUp()
        chrome_options = Options()
        # chrome_options.add_argument('--dns-prefetch-disable')
        # chrome_options.add_argument('--no-sandbox')
        # chrome_options.add_argument('--headless')
        # chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
    def tearDown(self):
        self.browser.quit()
        super(type(self), self).tearDown()

    def test_submit_form_and_delete(self):
        self.browser.get('http://localhost:8000/story_7/')
        self.assertNotIn('some text', self.browser.page_source)
        time.sleep(5)
        form_input = self.browser.find_element_by_id('input-status')
        form_input.send_keys('some text')
        button = self.browser.find_element_by_id('submit-status')
        button.click()
        time.sleep(5)
        self.browser.get('http://localhost:8000/story_7/')
        time.sleep(5)
        self.assertIn('some text', self.browser.page_source)
        del_button = self.browser.find_element_by_id('del-1')
        del_button.click()
        self.assertNotIn('some text', self.browser.page_source)

