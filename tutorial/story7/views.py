from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import Status
from .forms import StatusForm

# Create your views here.
def index(request):
    if request.POST:
        if request.POST['q'] == 'delete':
            status = get_object_or_404(Status, id=request.POST['id'])
            status.delete()
            return HttpResponse('ok')
        else :
            status_form = StatusForm(request.POST)
            status_form.save()
            return redirect('/story_7/')
    else:
        status_form = StatusForm()
        context = {'statuses' : Status.objects.all(), 'status_form' : status_form}
        return render(request, 'index__story7.html', context)