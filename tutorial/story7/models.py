from django.db import models

# Create your models here.
class Status(models.Model):
    content = models.CharField(max_length=300, null=False, blank=False)
    created_date = models.DateTimeField(auto_now_add=True)