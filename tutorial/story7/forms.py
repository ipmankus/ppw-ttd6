from django import forms
from django.forms import Textarea
from .models import Status

class StatusForm(forms.ModelForm):
    

    class Meta:
        model = Status
        fields = ["content",]
        widgets = {
            'content' : Textarea(attrs={'class': 'form-control', 'placeholder': '...', 'rows': '1', 'cols': '1', 'id': 'input-status'})
        }
