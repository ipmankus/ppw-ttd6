"""tutorial URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from story1.views import story_1
from django.conf.urls import include, url


urlpatterns = [
    path('admin/', admin.site.urls),
    path('story_1/', story_1, name='story_1'),
    path('story_4/', include('story4.urls'), name='story_4'),
    path('story_6/', include('story6.urls'), name='story_6'),
    path('story_7/', include('story7.urls', namespace='story7'), name='story_7'),
    path('story_8/', include('story8.urls'), name='story_8'),
    path('story_9/', include('story9.urls'), name='story_9'),
    path('story_10/', include('story10.urls'), name='story_10'),
    path('story_11/', include('story11.urls'), name='story_11'),
    url(r'', include('social_django.urls', namespace='social')),
]
