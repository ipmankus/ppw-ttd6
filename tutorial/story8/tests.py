from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time



class Story8ProfileTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_if_url_exist(self):
        response = self.client.get('/story_8/profile')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'profile__story8.html')


class E2ETest(TestCase):

    def setUp(self):
        super(E2ETest, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        super(E2ETest, self).tearDown()

    def test_change_theme(self):
        self.browser.get('http://localhost:8000/story_8/profile')
        time.sleep(5)
        button = self.browser.find_element_by_id('change-theme')
        button.click()
        time.sleep(5)
        self.assertIn('rgb(77, 0, 0);', self.browser.page_source)

    def test_accordion(self):
        self.browser.get('http://localhost:8000/story_8/profile')
        time.sleep(10)
        element_2_display = self.browser.find_element_by_id('ui-id-2').value_of_css_property('display')
        self.assertIn('none', element_2_display)

        button = self.browser.find_element_by_id('ac-1')
        button.click()
        time.sleep(5)

        element_2_display = self.browser.find_element_by_id('ui-id-2').value_of_css_property('display')
        self.assertIn('block', element_2_display)

    def test_loading_spinner(self):
        self.browser.get('http://localhost:8000/story_8/profile')
        spinner_display = self.browser.find_element_by_id('loading').value_of_css_property('display')
        self.assertIn('block', spinner_display)

        time.sleep(10)
        spinner_display = self.browser.find_element_by_id('loading').value_of_css_property('display')
        self.assertIn('none', spinner_display)