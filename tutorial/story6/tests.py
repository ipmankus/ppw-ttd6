from django.test import TestCase, Client
from .models import Status
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

# Testing welcome page
class Story6WelcomePageTestCase(TestCase):

    def setUp(self):
        self.client = Client()


    def test_if_url_exist(self):
        response = self.client.get('/story_6/')
        self.assertEqual(response.status_code, 200)


    def test_hello(self):
        response = self.client.get('/story_6/')
        self.assertIn(b'Hello, Apa kabar?', response.content)


    def test_create_status_success(self):
        response = self.client.post('/story_6/', data={'content':'a'*200})
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Status.objects.all())
        response = self.client.get('/story_6/')
        self.assertIn(b'a'*200, response.content)


    def test_create_status_fail(self):
        response = self.client.post('/story_6/', data={'content':'a'*301})
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Status.objects.all())
        response = self.client.get('/story_6/')
        self.assertNotIn(b'a'*301, response.content)


class Story6ProfileTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_if_url_exist(self):
        response = self.client.get('/story_6/profile')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'profile.html')


class E2ETest(TestCase):

    def setUp(self):
        super(E2ETest, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
        super(E2ETest, self).tearDown()

    def test_if_create_card_succeed(self):
        self.browser.get('http://localhost:8000/story_6/')
        input_box = self.browser.find_element_by_id('id_content')
        input_box.send_keys('Coba Coba')
        input_box.submit()
        time.sleep(5)
        self.assertIn('Coba Coba', self.browser.page_source)

    def test_if_create_card_fail(self):
        self.browser.get('http://localhost:8000/story_6/')
        input_box = self.browser.find_element_by_id('id_content')
        input_box.send_keys('C'*320)
        input_box.submit()
        time.sleep(5)
        self.assertNotIn('C'*320, self.browser.page_source)
