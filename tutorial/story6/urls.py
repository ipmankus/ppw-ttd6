from django.urls import path
from .views import welcome, profile

urlpatterns = [
    path('', welcome),
    path('profile', profile)
]