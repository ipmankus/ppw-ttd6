from django.test import TestCase, Client
from .models import JadwalPribadi

# Unit Test for testing HttpResponse and creating jadwal
class Story4TestCase(TestCase):


    def setUp(self):
        self.client = Client()


    def test_if_url_exist(self):
        response = self.client.get('/story_4/index.html')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/story_4/projects.html')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/story_4/register.html')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/story_4/articles.html')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/story_4/jadwal.html')
        self.assertEqual(response.status_code, 200)


    def test_create_jadwal_success(self):
        response = self.client.post('/story_4/jadwal.html', data={'name': 'isengserius',
            'date':'2018-10-11', 'time':'11:02', 'tempat': 'Fasilkom UI', 'kategori' : 'Cool'})
        self.assertEqual(response.status_code, 302)
        self.assertTrue(JadwalPribadi.objects.all())
        self.assertIsNotNone(JadwalPribadi.objects.get(pk=1))
        self.assertEqual(JadwalPribadi.objects.get(pk=1).name, "isengserius")


    def test_create_jadwal_fail(self):
        response = self.client.post('/story_4/jadwal.html', data={'name': 'isengserius',
            'date':'2018-13-11', 'time':'25:00', 'tempat': 'Fasilkom UI', 'kategori' : 'Cool'})
        self.assertEqual(response.status_code, 302)
        self.assertFalse(JadwalPribadi.objects.all())


    def test_delete_jadwal(self):
        response_0 = self.client.post('/story_4/jadwal.html', data={'name': 'isengserius',
            'date':'2018-10-11', 'time':'11:02', 'tempat': 'Fasilkom UI', 'kategori' : 'Cool'})
        self.assertEqual(response_0.status_code, 302)
        self.assertTrue(JadwalPribadi.objects.all())

        response_1 = self.client.post('/story_4/jadwal.html', data={'name': 'isengserius',
            'date':'2018-10-11', 'time':'11:02', 'tempat': 'Fasilkom UI', 'kategori' : 'Cool'})
        self.assertEqual(response_1.status_code, 302)
        self.assertTrue(JadwalPribadi.objects.all())

        response_delete = self.client.post('/story_4/delete_jadwal', data={'pk': '1'})
        self.assertEqual(response_delete.status_code, 302)
        self.assertTrue(JadwalPribadi.objects.all())


    def test_delete_all_jadwal(self):
        response_0 = self.client.post('/story_4/jadwal.html', data={'name': 'isengserius',
            'date':'2018-10-11', 'time':'11:02', 'tempat': 'Fasilkom UI', 'kategori' : 'Cool'})
        self.assertEqual(response_0.status_code, 302)
        self.assertTrue(JadwalPribadi.objects.all())

        response_1 = self.client.post('/story_4/jadwal.html', data={'name': 'isengserius',
            'date':'2018-10-11', 'time':'11:02', 'tempat': 'Fasilkom UI', 'kategori' : 'Cool'})
        self.assertEqual(response_1.status_code, 302)
        self.assertTrue(JadwalPribadi.objects.all())

        response_delete = self.client.get('/story_4/delete_jadwal_all')
        self.assertEqual(response_delete.status_code, 302)
        self.assertFalse(JadwalPribadi.objects.all())
