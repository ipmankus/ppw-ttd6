from django.urls import path
from .views import index, projects, register, articles, jadwal, delete_jadwal, delete_jadwal_all

urlpatterns = [
    path('index.html', index, name="index"),
    path('projects.html', projects, name="projects"),
    path('register.html', register, name="register"),
    path('articles.html', articles, name="articles"),
    path('jadwal.html', jadwal, name="jadwal"),
    path('delete_jadwal', delete_jadwal),
    path('delete_jadwal_all', delete_jadwal_all),
]