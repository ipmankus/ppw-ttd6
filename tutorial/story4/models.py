from django.db import models
from django.utils import timezone

# Create your models here.
class JadwalPribadi(models.Model):
    name = models.CharField(max_length=30)
    date = models.DateField()
    time = models.TimeField()
    tempat = models.CharField(max_length=30, default='Fasilkom UI')
    kategori = models.CharField(max_length=30, default='Cool')