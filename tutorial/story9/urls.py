from django.urls import path
from .views import books
from .views import proxy
from .views import books_challenge9
from .views import proxy_query

urlpatterns = [
    path('books', books),
    path('proxy', proxy),
    path('challenge9', books_challenge9),
    path('proxy-query', proxy_query)
]