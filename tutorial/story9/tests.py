from django.test import TestCase, Client

# Create your tests here.
class Story9TestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_if_url_exist(self):
        response = self.client.get('/story_9/books')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'books__story9.html')

        response = self.client.get('/story_9/proxy')
        self.assertEqual(response.status_code, 200)
