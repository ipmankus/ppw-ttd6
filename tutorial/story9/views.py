from django.shortcuts import render
from django.http import HttpResponse
import requests

# Create your views here.

def proxy(request):
    return HttpResponse(requests.
        get('https://www.googleapis.com/books/v1/volumes?q=quilting'))

def proxy_query(request):
    return HttpResponse(requests.
        get('https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q']))


def books(request):
    return render(request, 'books__story9.html')

def books_challenge9(request):
    return render(request, 'books__challenge9.html')