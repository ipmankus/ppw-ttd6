from django.test import TestCase, Client
from django.urls import resolve

# Create your tests here.
class Story1TestCase(TestCase):
    def test_url_story1_exists(self):
        response = Client().get('/story_1/')
        self.assertEqual(response.status_code, 200)

    def test_is_using_index_story1_template(self):
        response = Client().get('/story_1/')
        self.assertTemplateUsed(response, 'index_lab1.html')
