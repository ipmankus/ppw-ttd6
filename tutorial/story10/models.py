from django.db import models


# Create your models here.
class UserModel(models.Model):
    email = models.CharField(max_length=256, unique=True, blank=False, null=False)
    name = models.CharField(max_length=32, blank=False, null=False)
    password = models.CharField(max_length=128)
    salt = models.BinaryField(max_length=8)

    def as_dict(self):
        return {
            "name" : self.name,
            "email": self.email,
        }
