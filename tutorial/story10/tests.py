from django.test import TestCase, Client

# Create your tests here.
class Story10TestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_if_url_exist(self):
        response = self.client.get('/story_10/subscribe')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'subscribe__story10.html')

        response = self.client.get('/story_10/register')
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/story_10/unregister')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/story_10/subscriber-list')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/story_10/subscriber')
        self.assertEqual(response.status_code, 200)

    def test_get_subscriber_list(self):
        response = self.client.post('/story_10/register', data={
            'q':'create',
            'email':'nopsled@mail.ru',
            'name':'ramdhan',
            'password':'nopsled',
        })
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/story_10/subscriber-list')
        self.assertIn(b'ramdhan', response.content)

    def test_unregister(self):
        response = self.client.post('/story_10/register', data={
            'q':'create',
            'email':'nopsled@mail.ru',
            'name':'ramdhan',
            'password':'nopsled',
        })
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/story_10/unregister', data={
            'email':'nopsled@mail.ru',
            'password':'nopsled',
        })
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/story_10/subscriber-list')
        self.assertNotIn(b'ramdhan', response.content)

    def test_unregister_unsuccess(self):
        response = self.client.post('/story_10/register', data={
            'q':'create',
            'email':'nopsled@mail.ru',
            'name':'ramdhan',
            'password':'nopsled',
        })
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/story_10/unregister', data={
            'email':'nopsled@mail.ru',
            'password':'nop',
        })
        response = self.client.get('/story_10/subscriber-list')
        self.assertIn(b'ramdhan', response.content)

    def test_register(self):
        response = self.client.post('/story_10/register', data={
            'q':'create',
            'email':'nopsled@mail.ru',
            'name':'ramdhan',
            'password':'nopsled',
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"registered":true}')

        response = self.client.post('/story_10/register', data={
            'q':'create',
            'email':'nopsled@mail.ru',
            'name':'ramdhan',
            'password':'nopsled',
        })
        self.assertNotEqual(response.content, b'{"registered":true}')

    def test_ask_email(self):
        response = self.client.post('/story_10/register', data={
            'q':'askEmail',
            'email':'nopsled@mail.ru',
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"emailExist":false}')

        response = self.client.post('/story_10/register', data={
            'q':'create',
            'email':'nopsled@mail.ru',
            'name':'ramdhan',
            'password':'nopsled',
        })
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/story_10/register', data={
            'q':'askEmail',
            'email':'nopsled@mail.ru',
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"emailExist":true}')