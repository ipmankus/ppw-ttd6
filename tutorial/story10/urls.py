from django.urls import path
from .views import RegisterView, UnregisterView, subscribe, subscriber_list, subscriber

urlpatterns = [
    path('register', RegisterView.as_view()),
    path('subscribe', subscribe),
    path('subscriber-list', subscriber_list),
    path('subscriber', subscriber),
    path('unregister', UnregisterView.as_view()),
]