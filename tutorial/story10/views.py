import hashlib, binascii, os, re, json
from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from django.core.exceptions import ObjectDoesNotExist
from .models import UserModel


# Create your views here.
class RegisterView(View):


    def get(self, request):
        request = request.GET
        return HttpResponse('nice!')


    def post(self, request):
        request = request.POST
        if(request['q'] == 'askEmail'):
            try:
                UserModel.objects.get(email=request['email'])
                return HttpResponse('{"emailExist":true}')
            except ObjectDoesNotExist:
                return HttpResponse('{"emailExist":false}')
        elif(request['q'] == 'create'):
            salt = os.urandom(8)
            password = binascii.hexlify(hashlib.pbkdf2_hmac(
                'sha256',
                request['password'].encode(),
                salt,
                123
            ))
            try:
                if(re.search(r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', request['email'])):
                    UserModel.objects.create(
                        salt=salt,
                        password=password.decode(),
                        email=request['email'],
                        name=request['name'],
                    )
                    return HttpResponse('{"registered":true}')
            except:
                pass
            return HttpResponse('{"registered":false}')


def subscribe(request):
    return render(request, 'subscribe__story10.html')

def subscriber_list(request):
    dictionaries = [obj.as_dict() for obj in UserModel.objects.all()]
    return HttpResponse(json.dumps(dictionaries), content_type='application/json')

def subscriber(request):
    return render(request, 'subscriber__story10.html')

class UnregisterView(View):
    def post(self, request):
        request = request.POST
        try:
            user = UserModel.objects.get(email=request['email'])
            salt = user.salt
            password = binascii.hexlify(hashlib.pbkdf2_hmac(
                'sha256',
                request['password'].encode(),
                salt,
                123
            ))

            if(password.decode() == user.password):
                UserModel.objects.get(email=request['email'], password=user.password).delete()
                return HttpResponse('{"status": true}')
            else: raise Exception('wrong password')
        except:
            pass
        return HttpResponse('{"status": false}')
