from django.shortcuts import render, redirect
from django.http import HttpResponse

def home(request):
    if request.POST:
        request.session['star_array'] = request.POST['star_array']
    return render(request, 'home__story11.html')

def set_extra_data_on_session(backend, strategy, details, response,
    user=None, *args, **kwargs):
    if backend.name == 'google-oauth2':
        strategy.session_set('id_token', response['id_token'])
        strategy.session_set('email', response['emails'][0].get('value'))
        strategy.session_set('display_name', response['displayName'])
        strategy.session_set('image_url', response['image'].get('url'))
        strategy.session_set('star_array', '[]')

