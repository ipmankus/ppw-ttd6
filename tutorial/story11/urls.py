from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from .views import home

urlpatterns = [
    url('login', views.LoginView.as_view(template_name='login__story11.html'), name='story_11_login'),
    url('logout', views.LogoutView.as_view(next_page='login__story11.html'), name='story_11_logout'),
    url(r'^$', home, name='story_11_home'),
]
